#import "RNAirplayManager.h"
#import "RNAirplay.h"
#import <React/RCTBridge.h>
#import <React/UIView+React.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@implementation RNAirplayManager

RCT_EXPORT_MODULE();

@synthesize bridge = _bridge;

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

- (UIView *)view {

    MPVolumeView *volumeView = [[MPVolumeView alloc] init];
    volumeView.showsVolumeSlider = false;
    
    [volumeView setRouteButtonImage: [UIImage imageNamed:@"airplay.png"] forState:UIControlStateNormal];
    return volumeView;

}

RCT_EXPORT_METHOD(launchAirplayMenu)
{
    CGRect rect = CGRectMake(-100, 0, 0, 0);
    MPVolumeView *volumeView = [[MPVolumeView alloc] initWithFrame:rect];
    volumeView.showsVolumeSlider = false;
    UIView * currentView = [UIApplication sharedApplication].keyWindow.rootViewController.view;
    [currentView addSubview:volumeView];
    for(UIView * view in volumeView.subviews) {
        if([view isKindOfClass:[UIButton class]]) {
            UIButton * button = (UIButton *)view;
            [button sendActionsForControlEvents:(UIControlEventTouchUpInside)];
            break;
        }
    }
    [currentView willRemoveSubview:volumeView];
}

@end
